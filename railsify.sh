#!/bin/bash --login
RED='\033[0;31m'
NC='\033[0m' # No Color

source .envrc
 
echo ""
echo "********************"
echo "* RAILSIFY MY SITE *"
echo "*                  *"
echo "*   Archethought   *"
echo "*    Consulting    *"
echo "********************"
echo ""
echo "This program turns an HTML/CSS/JS template (like the ones from Wrap Bootstrap) it into a functional Rails 5 site."
echo ""

#------------------------------
#  Check program requirements
#------------------------------

# Check for RVM
command -v rvm >/dev/null 2>&1 || {
  printf "This program requires RVM to be installed on your system. Aborting.\n";
  printf "To install RVM, run this command:\n\\curl -L https://get.rvm.io | bash -s stable --ruby\n\n"
  exit 1;
}

# Check for pcregrep
command -v rvm >/dev/null 2>&1 || {
  printf "This program requires RVM to be installed on your system. Aborting.\n";
  printf "To install RVM, run this command:\n\\curl -L https://get.rvm.io | bash -s stable --ruby\n\n"
  exit 1;
}

#--------------------------------------------
#  Check ENV vars, or prompt for user input.
#--------------------------------------------

# Prompt for the Rails project name
if [ $PROJECT_NAME = "" ]; then
  printf "Choose a name for your project.\nThis will create a directory of the same name."
  printf "\n\nProject name: "
  read PROJECT_NAME
fi

# Prompt for the Ruby version
if [ $RUBY_VERSION = "" ]; then
  printf "Enter the version of Ruby to use: (e.g. 2.3.0)"
  printf "\n\nRuby version: "
  read RUBY_VERSION
fi

# Prompt for jQuery version
if [ $JQUERY_VERSION = "" ]; then
  printf "\n\nWhich version of JQuery (e.g. 2.2.3): "
  read JQUERY_VERSION
fi 

# Prompt for Bootstrap version
if [ $BOOTSTRAP_VERSION = "" ]; then
  printf "\n\nWhich version of Bootstrap (e.g. 3.3.6): "
  read BOOTSTRAP_VERSION
fi 

# Prompt for location of the HTML template for the index page
if [ $INDEX_TEMPLATE = "" ]; then
  printf "\n\nEnter the relative path to landing page HTML template: "
  read INDEX_TEMPLATE
fi 

# Prompt for the path to CSS files
if [ $CSS_PATH = "" ]; then
  printf "\nEnter the relative path to the template's CSS directory: "
  read CSS_PATH
fi

# Prompt for the path to Bootstrap files
if [ $BOOSTRAP_PATH = "" ]; then
  printf "\nEnter the relative path to the template's Bootstrap directory: "
  read BOOTSTRAP_PATH
fi

# Prompt for the path to JS files
if [ $JS_PATH = "" ]; then
  printf "\nEnter the relative path to the template's JS directory: "
  read JS_PATH
fi

# Prompt for path to font files
if [ $FONTS_PATH = "" ]; then
  printf "\nEnter the relative path to the template's fonts directory: "
  read FONTS_PATH
fi

# Ask for images path
if [ $IMAGES_PATH = "" ]; then
  printf "\nEnter the relative path to the template's images directory: "
  read IMAGES_PATH
fi

#------------------------------
# Ruby
#------------------------------
rubies=$(rvm list)
if (echo $rubies | grep -q "ruby-$RUBY_VERSION"); then
  printf "\nRuby $RUBY_VERSION already installed."
else
  printf "\nInstalling Ruby $RUBY_VERSION."
  rvm install $RUBY_VERSION
fi
rvm use $RUBY_VERSION
rvm current

#------------------------------
#  Rails
#------------------------------

# Install latest Rails 5 gem
gems=$(gem list)
if (echo $gems | grep -q "rails "); then
  printf "Rails is already installed.\n"
else
  printf "Installing Rails 5...\n"
  rvm @global do gem install rails --pre
fi

# Create Rails 5 project
if [ ! -d $PROJECT_NAME ]; then
  rails new $PROJECT_NAME
fi

cd $PROJECT_NAME

# add Bootstrap-sass gem
if (! grep -q "bootstrap-sass" Gemfile); then
  echo "gem 'bootstrap-sass', '~> 3.3.6'" >> Gemfile
fi

#if [ ! -f "Gemfile.lock" ]; then
  printf "\nInstalling gems..."
  bundle install
#else
#  printf "\nGems are already installed."
#fi

cd ..

# Copy assets into rails project
printf "\nCopying assets into Rails project."

cp -r $CSS_PATH/* $PROJECT_NAME/app/assets/stylesheets/
rm -r $PROJECT_NAME/app/assets/stylesheets/*.min.css

for file in $PROJECT_NAME/app/assets/stylesheets/*.css
do
  name=${file##*/}
  name="${name%.*}"
  mv $PROJECT_NAME/app/assets/stylesheets/$name\.css $PROJECT_NAME/app/assets/stylesheets/$name\.scss 
done

cp -r $JS_PATH/* $PROJECT_NAME/app/assets/javascripts/
rm -r $PROJECT_NAME/app/assets/javascripts/*.min.js

if [ ! -d "$PROJECT_NAME/app/assets/fonts" ]; then mkdir $PROJECT_NAME/app/assets/fonts; fi
cp -r $FONTS_PATH/* $PROJECT_NAME/app/assets/fonts/

cp -r $IMAGES_PATH/* $PROJECT_NAME/app/assets/images/

# Overwrite the Javascript manifest file
js_manifest_file=$PROJECT_NAME/app/assets/javascripts/application.js
echo "" | tee $js_manifest_file
echo "//= require jquery-$JQUERY_VERSION" >> $js_manifest_file
echo "//= require bootstrap" >> $js_manifest_file
echo "//= require plugins" >> $js_manifest_file
echo "//= require custom" >> $js_manifest_file

#read -d '' js_manifest <<"EOF"
#//= require jquery 
#//= require bootstrap-sprockets                                               
#//= require plugins                                                             
#//= require custom
#EOF
#echo "$js_manifest" | tee $PROJECT_NAME/app/assets/javascripts/application.js

printf "\nUpdating manifest files."
css_manifest_file=$PROJECT_NAME/app/assets/stylesheets/application.scss
echo "" | tee $css_manifest_file
echo "@import \"bootstrap-sprockets\";" >> $css_manifest_file                                                 
echo "@import \"bootstrap\";" >> $css_manifest_file      
                                           
for file in $CSS_PATH/*.css
do
  name=${file##*/}
  if grep -q "$name" $css_manifest_file
  then
    echo "$name already in manifest file."
  elif [[ $name =~ ".min.css" ]]
  then
    echo "$name is a minified duplicate...skipping."
  else
    echo "$name not in manifest file. Adding."
    name="${name%.*}"
    echo "@import \"$name\";" >> $css_manifest_file
  fi
done

# Create the pages controller
cd $PROJECT_NAME
printf "\nCreating PagesController."
if [ ! -f app/controllers/pages_controller.rb ]; then
read -d '' pages_controller <<"EOF"
class PagesController < ApplicationController

  def show
    begin
      render params[:id]
    rescue ActionView::MissingTemplate
      raise ActionController::RoutingError.new("No route matches [GET] \\"/#{params[:id]}\\"")
    end
  end
end
EOF
echo "$pages_controller" | tee app/controllers/pages_controller.rb
fi

# make the pages templates directory
printf "\nCreating app/views/pages directory."
[[ -d app/views/pages ]] || mkdir app/views/pages
cd ..
printf "\nCopying index template to app/views/pages/home.html.erb"
cp $INDEX_TEMPLATE $PROJECT_NAME/app/views/pages/home.html.erb

# Preserve only the markup inside of <body> from the html template
printf "\nRemoving extra markup from index template."
result=$(pcregrep -M '<body(\s*.*\s*)*</body>' $PROJECT_NAME/app/views/pages/home.html.erb)
echo "$result" > $PROJECT_NAME/app/views/pages/home.html.erb
sed -i '$d' $PROJECT_NAME/app/views/pages/home.html.erb
sed -i '1,1d' $PROJECT_NAME/app/views/pages/home.html.erb

# Change image paths
printf "\nEditing images paths for Rails asset pipeline."
sed -i 's/src="assets\/images\/\([^"]*\)"/src="<%= image_path(\"\1\") %>"/g' $PROJECT_NAME/app/views/pages/home.html.erb
sed -i 's/data-background="assets\/images\/\([^"]*\)"/data-background="<%= image_path(\"\1\") %>"/g' $PROJECT_NAME/app/views/pages/home.html.erb

# Remove statically referenced javascripts from template
sed -i '/<script src="assets/d' $PROJECT_NAME/app/views/pages/home.html.erb

# Use font path helpers in CSS
printf "\nFixing font paths in CSS."
for file in $PROJECT_NAME/app/assets/stylesheets/*.scss
do
  name=${file##*/}
  sed -i "s/url('\.\.\/fonts\/\([^']*\)')/font-url('\1')/g" $PROJECT_NAME/app/assets/stylesheets/$name
done

# add routes
if (grep -q "pages#home" $PROJECT_NAME/config/routes.rb); then
printf "\nPagesController routes already specified in config/routes.rb."
else
printf "\nAdding PagesController routes to config/routes.rb."
head -n -1 $PROJECT_NAME/config/routes.rb > /tmp/routes.rb ; mv /tmp/routes.rb $PROJECT_NAME/config/routes.rb
read -d '' pages_routes <<"EOF"
  
  root to: 'pages#home', id: 'home'
  get '/:id', to: 'pages#show', as: :pages
end
EOF
echo "$pages_routes" >> $PROJECT_NAME/config/routes.rb
fi
 
printf "\n"
