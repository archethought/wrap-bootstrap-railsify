Takes a static HTML/CSS/JS site and turns it into a simple Rails site.

To run, ensure the script is executable and run it:

    ./railsify.sh

If you want to skip the prompts, you can define variables in your environment
that the script will use if provided:

    export RUBY_VERSION=2.3.0
    export PROJECT_NAME=my-project
    export JQUERY_VERSION=2.2.3
    export BOOTSTRAP_VERSION=3.3.6
    export INDEX_TEMPLATE=Neomax/Site/index-onepage.html
    export CSS_PATH=Neomax/Site/assets/css
    export JS_PATH=Neomax/Site/assets/js
    export FONTS_PATH=Neomax/Site/assets/fonts
    export IMAGES_PATH=Neomax/Site/assets/images
